﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using WebKazmirenkoStudio.Data;
using WebKazmirenkoStudio.Model;
using WebKazmirenkoStudio.Properties.Resources;

namespace WebKazmirenkoStudio.Utils.Event
{
    /// <summary>
    /// Расчет стоимости сырья с учетом стоимостисырья
    /// </summary>
    public class CalculationCostRawMaterialsWithDelivery
    {
        #region Method public 

        /// <summary>
        /// Расчет
        /// </summary>
        /// <param name="purchaseId">ID закупки</param>
        public void Calculation(Guid purchaseId)
        {
            CheckParam(purchaseId);
            float priceDelivery = GetPriceDelivery(purchaseId);
            SetCalculationCost(purchaseId, priceDelivery);
        }
        
        /// <summary>
        /// Расчет
        /// </summary>
        /// <param name="purchaseId">ID закупки</param>
        /// <param name="priceDelivery">Цена доставки</param>
        public void Calculation(Guid purchaseId, float priceDelivery)
        {
            CheckParam(purchaseId);
            SetCalculationCost(purchaseId, priceDelivery);
        }

        #endregion

        #region Method private

        /// <summary>
        /// Расчитать стоимость доставки сырья 
        /// </summary>
        /// <param name="purchaseId">ID закупки</param>
        /// <param name="priceDelivery">Цена доставки</param>
        void SetCalculationCost(Guid purchaseId, float priceDelivery)
        {
            List<RawMaterialModel> rawMaterials = GetRawMaterialCount(purchaseId);

            if (rawMaterials.Count.Equals(0) || priceDelivery.Equals(float.MinValue))
                return;

            using var ctx = new WebKazmirenkoStudioContext();

            foreach (RawMaterialModel entity in rawMaterials)
            {
                float shippingCostPerPiece = (float)((priceDelivery / rawMaterials.Count) / entity.Quantity);
                ctx.RawMaterial.Find(entity.Id).ShippingCostPerPiece = shippingCostPerPiece;
                ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Получение количества сырья в закупке
        /// </summary>
        /// <param name="purchaseId">ID закупки</param>
        /// <returns>Количество сырья в закупке</returns>
        List<RawMaterialModel> GetRawMaterialCount(Guid purchaseId)
        {
            using var ctx = new WebKazmirenkoStudioContext();
            return ctx.RawMaterial.Where(x => x.Purchase.Id.Equals(purchaseId)).ToList();
        }

        /// <summary>
        /// Получить цену доставки
        /// </summary>
        /// <param name="purchaseId">ID закупки</param>
        float GetPriceDelivery(Guid purchaseId)
        {
            using var ctx = new WebKazmirenkoStudioContext();
            Purchase purchase = ctx.Purchase.Where(x => x.Id.Equals(purchaseId)).FirstOrDefault();

            return purchase != default ? purchase.PriceDelivery : float.MinValue;
        }

        /// <summary>
        /// Проверка что поле purchaseId заполнено
        /// </summary>
        Action<Guid> CheckParam = purchaseId =>
        {
            if (purchaseId.Equals(Guid.Empty))
                throw new Exception(ErrorResource.PurchaseIdIsEmpty);
        };

        #endregion
    }
}
